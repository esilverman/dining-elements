
<!doctype html>
<html>
	<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="description" content="DiningElements.com - Corporate Aviation & Yacht Dining Essentials">
	<title>DiningElements.com</title>
	<link rel="stylesheet" href="de.css" type="text/css"> 
</head>
<body>
<div id="wrapper">
	<div id="header" class="center">Dining Elements</div>
	<div id="subHeader" class="center">Corporate Aviation & Yacht Dining Essentials</div>
	<div id="contact" class="center">Dana Robbin / 1+203.249.1164 / <a href="mailto:DanaRobbin@DiningElements.com">DanaRobbin@DiningElements.com</a></div>
	<div id="brochureFront" class="brochure"></div>
	<div id="brochureBack" class="brochure"></div>
</div>
</body>
</html>